﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringTest : MonoBehaviour
{
    [SerializeField]
    private GameObject targetObj;
    [SerializeField]
    private GameObject chaseObj;
    [SerializeField]
    private Camera chaseFocusCamera;

    bool isChase;
    float moveSpeed = 1.0f;
    Vector3 prevPosition;
    Vector3 targetVelocity;
    float targetDistance;
    float targetVelocityMagnitude;
    float targetVelocityScale;

    Vector3 desireVelocity;
    Vector3 steeringForce;

    Vector3 chaserVelocity;
    Vector3 chaserPrevPosition;

    Vector3 chaserOffset;

    private void Start()
    {
        targetObj.GetComponent<MeshRenderer>().material.color = new Color(1.0f, 0.0f, 0.0f);
        isChase = false;

        chaserOffset = chaseFocusCamera.transform.position - chaseObj.transform.position;
    }

    private void Update()
    {
        if(isChase)
        {
            targetVelocity = targetObj.transform.position - prevPosition;
            targetDistance = (targetObj.transform.position - chaseObj.transform.position).magnitude;
            if(targetDistance < 5.0f)
            {
                isChase = false;
                return;
            }
            targetVelocityMagnitude = targetVelocity.magnitude;
            targetVelocityScale = targetDistance / moveSpeed;

            desireVelocity = (targetObj.transform.position + (targetVelocity * targetVelocityScale) - chaseObj.transform.position).normalized * moveSpeed;
            chaserVelocity = (chaseObj.transform.position - chaserPrevPosition).normalized * moveSpeed;
            steeringForce = Vector3.ClampMagnitude(desireVelocity - chaserVelocity, moveSpeed * 0.1f);
            
            prevPosition = targetObj.transform.position;
            chaserPrevPosition = chaseObj.transform.position;

            chaseObj.transform.position += (steeringForce + chaserVelocity);
            Debug.Log("DesireVelocity : " + desireVelocity + " SteeringForce : " + steeringForce + " ChaserVelocity : " + chaserVelocity);

            chaseFocusCamera.transform.position = chaseObj.transform.position + chaserOffset;
        }
    }

    public void TargetRandom()
    {
        targetObj.transform.localPosition = new Vector3(Random.Range(-50.0f, 50.0f), 0.0f, Random.Range(-50.0f, 50.0f));
        isChase = true;
    }
}
